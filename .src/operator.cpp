#include <iostream>
#include <utility>
#include <cmath>
#include "math.h"
using namespace std;

class OrdPair {
	public:
		OrdPair( ) {  //default constructor to initialize values
			p1 = 0.0;
			p2 = 0.0;
		}
		OrdPair( float f1 , float f2 ): p1(move(f1)), p2(move(f2)) {} //constructor

		OrdPair operator!();
		bool operator==(const OrdPair& ) const;  //prototypes
		OrdPair operator/(const OrdPair& ) const;
		OrdPair operator*(const OrdPair& ) const;
		OrdPair operator-(const OrdPair& ) const;
		OrdPair operator+(const OrdPair& ) const;
		void write_it( ) const;
	private:
		float p1, p2;
};

OrdPair OrdPair::operator!() {
	p1= (-1)*p1;
	p2= (-1)*p2;

	return  OrdPair(p1,p2);
}
bool OrdPair::operator==(const OrdPair& s) const {
	return p1 == s.p1 && p2 == s.p2;
}

OrdPair OrdPair::operator/( const OrdPair& s) const {
	OrdPair z(p1 / s.p1, p2 / s.p2);
	return z;
}

OrdPair OrdPair::operator*( const OrdPair& s) const { //use const to avoid altering member values
	OrdPair z(p1 * s.p1, p2 * s.p2);
	return z;
}

OrdPair OrdPair::operator-( const OrdPair& s) const {
	OrdPair z(p1 - s.p1, p2 - s.p2);
	return z;
}

OrdPair OrdPair::operator+( const OrdPair& s) const {
	OrdPair z(p1 + s.p1, p2 + s.p2);
	return z;
}

void OrdPair::write_it( ) const {
	cout << "The result is: (" << p1 << "," << p2 << ")" << endl;
}

int main() {
	OrdPair s1(32, 2), s2(32,-2), s3;

	cout<<" (s1) | ";
	s1.write_it();
	cout<<" (s2) | ";
	s2.write_it();

	cout<<" (=) | ";
	if(s1.operator==(s2)) cout << "equal" << endl;
	// or can use:  if (s1 == s2)
	else cout<< "Not equal" << endl;

	// s3 = s1.operator/(s2);
	s3 = s1 / s2;
	cout<<" (/) | ";
	s3.write_it();

	// s3 = s1.operator*(s2);
	s3 = s1 * s2;
	cout<<" (*) | ";
	s3.write_it();

	// s3 = s1.operator-(s2);
	s3 = s1 - s2;
	cout<<" (-) | ";
	s3.write_it();

	// s3 = s1.operator+(s2);
	s3 = s1 + s2;
	cout<<" (+) | ";
	s3.write_it();

	// s3 = s1.operator!(s2);
	cout<<" (!) | ";
	s3= s1 ! s2;
	s3.write_it();

	return 0;
}
